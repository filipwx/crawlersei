#!/usr/bin/env python3

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
import time
import datetime
import sys
import platform
import os
import calendar
import re
import urllib.request
import urllib.parse
import http.cookiejar
import scrapy
import json
from scrapy import Selector
from pymongo import MongoClient

client = MongoClient('mongodb+srv://videouser:1234video1234@cluster0.ro7ig.mongodb.net/video?retryWrites=true&w=majority')
collec_dados = client.video.dados


class CrunchbaseSpider(scrapy.Spider):
    name = 'crunchbase'
    start_urls = ['https://www.google.com.br']

    def __init__(self):
        options = Options()
        options.headless = False
        options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36")
        # self.driver = webdriver.Firefox(options=options, executable_path=r'drivers/geckodriver')
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)

    def paginar(self, data):
        for row in data:
            name = row.find_element_by_css_selector(
                ".flex-no-grow.cb-overflow-ellipsis.identifier-label")
            name = name.get_attribute('innerHTML').strip()
            industries = row.find_element_by_css_selector(
                'span.component--field-formatter.field-type-identifier-multi a').text
            description = row.find_element_by_css_selector(
                'span.component--field-formatter.field-type-text_long.ng-star-inserted').text
            location = row.find_element_by_css_selector(
                'grid-cell[data-columnid="location_group_identifiers"] a.link-accent.ng-star-inserted').text            

            try:
                link = row.find_element_by_css_selector('link-formatter.ng-star-inserted a').text
                pass
            except:
                link = '-'
                pass

            try:
                revenue = row.find_element_by_css_selector(
                    'grid-cell[data-columnid="revenue_range"] a.link-accent.ng-star-inserted').text
                pass
            except:
                revenue = 0
                pass

            try:
                status = row.find_element_by_css_selector(
                    'grid-cell[data-columnid="operating_status"] span.ng-star-inserted').text
                pass
            except:
                status = 0
                pass

            try:
                founded_on = row.find_element_by_css_selector(
                    'grid-cell[data-columnid="founded_on"] span.ng-star-inserted').text
                pass
            except:
                founded_on = 0
                pass

            try:
                contact_email = row.find_element_by_css_selector(
                    'grid-cell[data-columnid="contact_email"] span.ng-star-inserted').text
                pass
            except:
                contact_email = 0
                pass

            try:
                phone_number = row.find_element_by_css_selector(
                    'grid-cell[data-columnid="phone_number"] span.ng-star-inserted').text
                pass
            except:
                phone_number = 0
                pass

            payload = {
                'name': name,
                'industries': industries,
                'description': description,
                'location': location,
                'link': link,
                'revenue': revenue,
                'status': status,
                'founded_on': founded_on,
                'contact_email': contact_email,
                'phone_number': phone_number,
            }
            collec_dados.find_one_and_update({'link': payload['link']}, {'$set': dict(payload)}, upsert=True)
            print('\n')
        return True
        pass
        

    def parse(self, response):
        self.driver.get('https://www.crunchbase.com/search/organization.companies/field/hubs/org_num/berlin-commerce-companies')
        time.sleep(10)
        self.driver.maximize_window()
        time.sleep(2)

        login = self.driver.find_element_by_xpath('/html/body/chrome/div/app-header/div[1]/div[2]/div/anon-nav-row/nav-action-item[2]/nav-header-action-item/a')
        login.click()
        time.sleep(1)

        email = self.driver.find_element_by_xpath('//*[@id="mat-input-3"]')
        email.send_keys("filipwx7@gmail.com")
        time.sleep(1)

        senha = self.driver.find_element_by_xpath('//*[@id="mat-input-4"]')
        senha.send_keys("Hesoyam199")
        time.sleep(1)

        entrar = self.driver.find_element_by_xpath('//*[@id="mat-tab-content-0-0"]/div/login/form/button')
        entrar.click()
        time.sleep(12)

        editv = self.driver.find_element_by_xpath('/html/body/chrome/div/mat-sidenav-container/mat-sidenav-content/div/search/page-layout/div/div/form/div[2]/results/div/div/div[1]/div/div/div/button[1]')
        editv.click()
        time.sleep(2)

        check1 = self.driver.find_element_by_xpath('//*[@id="mat-checkbox-70"]/label')
        check1.click()
        time.sleep(2)

        check2 = self.driver.find_element_by_xpath('//*[@id="mat-checkbox-72"]/label')
        check2.click()
        time.sleep(2)

        check3 = self.driver.find_element_by_xpath('//*[@id="mat-checkbox-74"]/label')
        check3.click()
        time.sleep(2)

        check7 = self.driver.find_element_by_xpath('//*[@id="mat-checkbox-75"]/label')
        check7.click()
        time.sleep(2)

        html = self.driver.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(2)

        check4 = self.driver.find_element_by_xpath('//*[@id="mat-checkbox-79"]/label')
        check4.click()
        time.sleep(2)

        check5 = self.driver.find_element_by_xpath('//*[@id="mat-checkbox-83"]/label')
        check5.click()
        time.sleep(2)

        check6 = self.driver.find_element_by_xpath('//*[@id="mat-checkbox-84"]/label')
        check6.click()
        time.sleep(2)

        confirm = self.driver.find_element_by_xpath('//*[@id="mat-dialog-0"]/column-panel/div/dialog-layout/div/mat-dialog-actions/div/button')
        confirm.click()
        time.sleep(19)

        lista = self.driver.find_element_by_xpath('/html/body/chrome/div/mat-sidenav-container/mat-sidenav-content/div/search/page-layout/div/div/form/div[2]/results/div/div/div[3]/sheet-grid/div/div/grid-body/div')
        rows = lista.find_elements(By.TAG_NAME, "grid-row")
        rows.pop(0)
        self.paginar(rows)
        paginator = True
        time.sleep(8)
        while paginator:
            try:
                self.driver.find_element_by_css_selector('results-info.show-gt-xs h3.component--results-info a[aria-disabled="false"]:nth-child(2)').click()
                time.sleep(15)
                lista = self.driver.find_element_by_xpath('/html/body/chrome/div/mat-sidenav-container/mat-sidenav-content/div/search/page-layout/div/div/form/div[2]/results/div/div/div[3]/sheet-grid/div/div/grid-body/div')
                rows = lista.find_elements(By.TAG_NAME, "grid-row")
                rows.pop(0)
                self.paginar(rows)
                time.sleep(9)
                pass
            except:
                paginator = False
                pass
            pass
        self.driver.quit()
        pass
